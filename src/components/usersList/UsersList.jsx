import React from "react";
import "./usersList.css";
import PropTypes from "prop-types";

export const UsersList = ({ activeUsersList, clickDeleteHandler }) => {
  return (
    <div>
      {activeUsersList.lenght !== 0 ? (
        <h1>пользователей нет</h1>
      ) : (
        activeUsersList.map((user) => {
          return (
            <div key={user.id}>
              <p>{user.name}</p>
              <p>{user.phone}</p>
              <button
                onClick={() => clickDeleteHandler(activeUsersList, user.id)}
              >
                Удалить
              </button>
            </div>
          );
        })
      )}
    </div>
  );
};

UsersList.propTypes = {
  someprop: PropTypes.string,
};
