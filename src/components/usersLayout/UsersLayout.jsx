import React, { useState, useEffect } from "react";
import "./usersLayout.css";
import PropTypes from "prop-types";
import { UserButton } from "../userButton/UserButton";
import { UsersList } from "../usersList/UsersList";
import { UsersStats } from "../usersStats/UsersStats";

export const UsersLayout = () => {
  const [users, setUsers] = useState([]);
  const [activeUsers, setActiveUsers] = useState([]);
  const [addedUsers, setAddedUsers] = useState(0);
  const [deletedUsers, setDeletedUsers] = useState(0);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then((response) => response.json())
      .then((json) => setUsers(json));
  }, []);

  const addUser = (user) => {
    setActiveUsers([...activeUsers, user]);
    setAddedUsers((prevState) => prevState + 1);
  };

  const deleteUser = (usersArray, id) => {
    console.log(id);
    setActiveUsers(usersArray.filter((user) => user.id !== id));
    setDeletedUsers((prevState) => prevState + 1);
  };

  return (
    <div>
      <UsersList
        activeUsersList={activeUsers}
        clickDeleteHandler={deleteUser}
      />
      <UsersStats added={addedUsers} deleted={deletedUsers} />
      {users.map((user) => (
        <UserButton key={user.id} userData={user} clickHandler={addUser} />
      ))}
    </div>
  );
};

UsersLayout.propTypes = {
  someprop: PropTypes.string,
};
