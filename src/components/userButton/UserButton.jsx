import React, { useState } from "react";
import "./userButton.css";
import PropTypes from "prop-types";

export const UserButton = ({ userData, clickHandler }) => {
  const [isDisabled, setIsDisabled] = useState(false);

  return (
    <button
      disabled={isDisabled}
      onClick={() => {
        clickHandler(userData);
        setIsDisabled(true);
      }}
    >
      {userData.name}
    </button>
  );
};

UserButton.propTypes = {
  someprop: PropTypes.string,
};
