import React, { useState } from "react";
import "./userStats.css";
import PropTypes from "prop-types";

export const UsersStats = ({ added, deleted }) => {
  return (
    <div>
      <p>Добавлено пользователей: {added}</p>
      <p>Удалено пользователей: {deleted}</p>
    </div>
  );
};

UsersStats.propTypes = {
  someprop: PropTypes.string,
};
