import './App.css';
import { UsersLayout } from './components/usersLayout/UsersLayout';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <UsersLayout/>
      </header>
    </div>
  );
}

export default App;
